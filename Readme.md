#Description
* This image will download a JAR artifact at $ARTIFACT_URL and install it in /opt.
* You can specify an $ARTIFACT_USERNAME/$ARTIFACT_PASSWORD to access $ARTIFACT_URL if needed (Basic http authentication).
* If you specify $POSTGRES_HOST, $POSTGRES_USERNAME and $POSTGRES_PASSWORD, container will start JAR app only when database connection become available

#Usages:
* $ARTIFACT_URL=http://my-artifactory.com/artifactory/libs-snapshot-local/com/company/software/1.0.0-SNAPSHOT/software-1.0.0-SNAPSHOT.jar
* $ARTIFACT_USERNAME=username (optional)
* $ARTIFACT_PASSWORD=password (optional)
* $POSTGRES_HOST=postgres_host_to_check_availability (optional)
* $POSTGRES_PORT=postgres_port_to_check_availability (optional)
* $POSTGRES_NAME=postgres_name_to_check_availability (optional)
* $POSTGRES_USERNAME=postgres_username_to_check_availability (optional)
* $POSTGRES_PASSWORD=postgres_password_to_check_availability (optional)


docker run -it -eARTIFACT_URL="http://artifactory.arxcf.com/artifactory/libs-snapshot-local/com/arx/oauth/0.0.1-SNAPSHOT/oauth-0.0.1-20181122.142712-1.jar" \
    -eARTIFACT_USERNAME=admin \
    -eARTIFACT_PASSWORD=Arx12345 \
    -ePOSTGRES_HOST=localhost \
    -ePOSTGRES_PORT=5432 \
    -ePOSTGRES_NAME=oauth \
    -ePOSTGRES_USERNAME=postgres \
    -ePOSTGRES_PASSWORD=manager \
    artifact-java
    
docker run -it -eARTIFACT_URL="http://artifactory.arxcf.com/artifactory/libs-snapshot-local/com/arx/oauth/0.0.1-SNAPSHOT/oauth-0.0.1-20181122.142712-1.jar" -eARTIFACT_USERNAME=admin -eARTIFACT_PASSWORD=xxx chaya56/artifact-java